import { Component, OnInit } from '@angular/core';
import { User } from '../user';
import { FormGroup,FormControl } from '@angular/forms';

@Component({
  selector: 'app-reactive-forms',
  templateUrl: './reactive-forms.component.html',
  styleUrls: ['./reactive-forms.component.css']
})
export class ReactiveFormsComponent implements OnInit {

  userList: User[]=[];

  form: FormGroup;

  addUser(form){
    console.log(form.value);
  }
  constructor() { }

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl(''),
      contact: new FormControl(''),
      email: new FormControl(''),
    })
  }

}
