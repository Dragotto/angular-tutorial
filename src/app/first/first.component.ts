import {Component} from '@angular/core';

@Component({
    selector: "app-first",
    templateUrl: 'first.component.html',
    styleUrls: ['first.component.css']
  })


export class FirstComponent{
  interpolation:string='I love interpolation';
  imageBinding:string='../../assets/blog.jpg';

  returnString(){
    return 'I love function returninterpolation';
  }

  changeValue(){
    this.interpolation = 'this value has been changed with event binding!';
  }

}