import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-gotcharacter',
  templateUrl: './gotcharacter.component.html',
  styleUrls: ['./gotcharacter.component.css']
})
export class GOTCharacterComponent implements OnInit {
GOTCharacter:string[];
GOTCharacterFirstName:string[]=['John','Axia','Cruzzo','Mongol'];
GOTCharacterLastName:string[]=['JohnChange','AxiaChange','CruzzoChange','MongolChange'];
i=0;
stopSwitch:any;
stopSwitchStatus=true;
twoWayData='TwoWay';

StopSwitch(){
clearInterval(this.stopSwitch);
this.stopSwitchStatus=true;
}

SwitchFIrstAndLastName(){
  this.stopSwitch=setInterval(()=>{
    if(this.i%2==0){
      this.GOTCharacter=this.GOTCharacterFirstName;
      console.log('if');
    }else{
      this.GOTCharacter=this.GOTCharacterLastName;
      console.log('else');
    }
  this.i++;
    }, 300);
    this.stopSwitchStatus=false;
}

constructor() { 
  this.GOTCharacter=this.GOTCharacterFirstName;
}

  ngOnInit() {

  }

}
